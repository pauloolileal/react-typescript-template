import React from "react";
import logo from "./logo.png";
import "./Home.scss";
import { QueryStringService as _queryStringService } from "../../services/queryStringService/QueryStringService";
import { CurrentEnvironment, CurrentLogLevel } from "../../configs/Environment";
import { _logPage } from "../../configs/LogConfig";
import { Json } from "../../utils/Json";
import { Button, ButtonGroup } from "@mui/material";

const _logger = _logPage.getChildCategory("App");

function RegisterLog():void{
  _logger.info("Hey! Log registered!")
}

// Exemple function to get AppData from query string
// For tests, add in the url the following params:
// ?id=1&name=teste&value=value
function GetQueryString(): void {
  var appData = _queryStringService.Parse();

  _logger.info(Json.StringifyFormat(appData));
}

function Home(): JSX.Element {
  return (
    <div className="app">
      <header className="app-header">
        <img src={logo} className="app-logo" alt="logo" />
        <p>Start to develop your own project!</p>
        <a
          className="app-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <a
          className="app-link"
          href="https://mui.com/pt/material-ui/getting-started/overview/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Material UI
        </a>
        <p>
          <b>Current environment: </b>
          {CurrentEnvironment}
        </p>
        <p>
          <b>Current log level: </b>
          {CurrentLogLevel}
        </p>
        <ButtonGroup variant="contained">
          <Button
            onClick={() => RegisterLog()}
          >
            Register log
          </Button>
          <Button
            onClick={() => GetQueryString()}
          >
            Get AppData from Query String
          </Button>
        </ButtonGroup>
      </header>
    </div>
  );
}

export default Home;
