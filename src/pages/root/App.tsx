import React from "react";
import "./App.scss";
import { _logMain } from "../../configs/LogConfig";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "../home/Home";
import { CurrentEnvironment, CurrentLogLevel } from "../../configs/Environment";

const _logger = _logMain.getChildCategory("App");

_logger.info(`Current environment: ${CurrentEnvironment}`);
_logger.info(`Current log level: ${CurrentLogLevel}`);

function App(): JSX.Element {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/home" element={<Home />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
