export const LogLevels = {
  Trace: "Trace",
  Debug: "Debug",
  Info: "Info",
  Warn: "Warn",
  Error: "Error",
  Fatal: "Fatal",
};
