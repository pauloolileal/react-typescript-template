export class Json {
    static StringifyFormat(value: any): string {
        return JSON.stringify(value, null, 4);
    }
}