import { _logService } from "../../configs/LogConfig";
import parser from "query-string";
import { Json } from "../../utils/Json";

const _logger = _logService.getChildCategory("QueryString");

function Parse(): object {
  let queryString: string = window.location.search;

  _logger.info(`Parsing "${queryString}"...`);

  let result = parser.parse(queryString);

  _logger.info(`Parsed "${queryString} with success"`);
  _logger.debug(Json.StringifyFormat(result), "Parsed object");

  return result;
}

export const QueryStringService = {
  Parse,
};
