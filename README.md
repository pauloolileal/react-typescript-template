[TOC]

# Descrição
Esse templete será utilizado para criação de projetos _react_ utilizando _typescript_.

O template está preparado para ser utilizado para vários ambientes (_development_, _staging_ e _production_). Todo o tutorial de utilização e construção de pipelines está descrito abaixo.

Já trás também exemplos de serviços, implementando boas práticas de desenvolvimento.

# Como utilizar

Para iniciar a utilização do template é muito simples:
1. Crie (ou solicite) a criação do repositório _GIT_
2. Clone esse repositório de template
3. Mude a _origin_ para o seu repositório, com o comando:
```bash
git remote set-url origin <URL_DO_SEU_REPOSITORIO>
```
4. Abra o _Notepad++_
5. Aperte as teclas `CTRL + H`
6. Selecione a aba "Localizar nos arquivos"
   - Em "Localizar", digite "react-typescript-template"
   - Em "Substituir por", digite "NOME-DA-SUA-APLICAÇÃO"
   - Em "Pasta", navegue até o diretório raiz do projeto
7. Renomeie a pasta clonada para o "NOME-DA-SUA-APLICAÇÃO"
9. Publique as alteações no _GIT_, com os comandos
```bash
git add .
git commit -m "Adicionado template do projeto"
git push origin master
```
10. Para executar o projeto, use os [Comandos disponíveis](#comandos-disponíveis).
11. Para criar o pipeline, siga o [Tutorial](#pipeline) abaixo.

> __Sempre siga as boas práticas de código aplicadas no projeto.__

# Comandos disponíveis

> Os comandos de _staging_ e _production_ serão mais utilizados no pipeline.

## Comandos para ambiente de _development_
- `npm run start`: para rodar à aplicação em desenvolvimento

- `npm run build`: para compilar à aplicação em desenvolvimento

- `npm run test` para executar os testes em desenvolvimento

## Comandos para ambiente de _staging_
- `npm run start:staging`: para rodar à aplicação em homologação

- `npm run build:staging`: para compilar à aplicação em homologação

- `npm run test:staging`: para executar os testes em homologação

## Comandos para ambiente de _production_
- `npm run start:production`: para rodar à aplicação em produção

- `npm run build:production`: para compilar à aplicação em produção

- `npm run test:production`: para testar à aplicação em produção

## Outros comandos
- `npm run format`: para formatar os arquivos
- `npm run lint`: para executar o `lint`

# Environments (ambientes)
As variáveis de ambiente, que podem ser setadas via pipeline, estão divididas nos arquivos `.env`. Existe uma para cada ambiente.

- `.env.development`: para variáveis do ambiente de desenvolvimendo
- `.env.staging`: para variáveis do ambiente de homologação
- `.env.production`: para variáveis do ambiente de produção

> As variáveis de _environment_ (_staging_ e _production_) devem ser substituidas durante a execução do pipeline

## Variáveis _defaults_

As variáveis defaults são as que precisam existir em todos os ambientes, são elas:
- `REACT_APP_ENVIRONMENT`: define qual ambiente está sendo executado. Os valores permitidos são: _development_, _staging_ e _production_.
- `REACT_APP_LOG_LEVEL`: define qual nível de log deve ser exibido na execução. Os valores permitidos são: `Trace`, `Debug`, `Info`, `Warn`, `Error` e `Fatal`.
> Atualmente os ambientes de _development_ está definido como `Trace`, _staging_ como `Debug` e _production_ como `Error`.

## Como utilizar as variáveis

Para utilizar é necessário que ela seja adicionada ao arquivo `.env` e também ao `Environment.ts`.

No arquivo `.env` é imprecindível que a variável comece com `REACT_APP_...`. Por exemplo a variável de environment é definida como: `REACT_APP_ENVIRONMENT`.

No arquivo de `Environment.ts`, deve ser definida como uma constante global, exemplo: 

```typescript
export const CurrentEnvironment = process.env.REACT_APP_ENVIRONMENT;
```

A partir daí a utilização em outros arquivos será feita importando a constante, por exemplo: 

```typescript
...
import { CurrentEnvironment } from "../../configs/Environment";
...
```

# Logs

Para facilitar o desenvolvimento e evitar "sugeiras" ao longo do desenvolvimento, foi adicionado uma biblioteca de logs. __Portanto não utilize `console.log`, dê preferencia ao uso de logs.__

Os níveis de logs, são: 
- `Trace`: representa todos os detalhes para diagnostico
- `Debug`: representa informações para diagnóstico
- `Info`: representa qualquer informação relevante a ser exibida
- `Warn`: representa problemas ou situações incomuns. Funciona mais como avisos
- `Error`: representa um problema sério mas que não quebra à aplicação
- `Fatal`: representa situações catastóficas

É importante que o nível de log seja usado corretamente, impedindo que informações não necessárias sejam exibidas em ambientes produtivos.

## Utilizando os logs

Os logs são rastreaveis a partir da origem de cada um, formando um caminho para o rastreamento. Atualmente existem as catégorias de `Main`, `Page`, `Model` e `Service`. Portanto devem ser utilzado a categoria correta na hora de utilizar o log.

É possível importar cada um respectivamente: `_logMain`, `_logPage`, `_logModel`, `_logService`

Para a utilização é necessário importar, no arquivo desejado a seguinte a categoria de log correta, supondo que estejamos _logando_ um serviço:

```typescript
...
import { _logService } from "../../configs/LogConfig";
...
```

De forma global no serviço crie uma constate de log:

```typescript
...
const _logger = _logService.getChildCategory("Service");
...

```

A partir daí é só utilizar os log, por exemplo:

```typescript
...
_logger.info(`Registering log | ${eventName}: ${value}`);
_logger.debug(Json.StringifyFormat(request), "Request body.");
...
```
Como pode ser visto, para informação é só mostrado que está registrando o tracking e para debug é exibido o objeto da requisição serializado.

No console será exibido a seguinte mensagem:
```json
2022-09-28 16:48:05,904 INFO  [service#Service] Registering service | teste: teste
2022-09-28 16:48:05,904 DEBUG [service#Service] {
    "id": "1",
    "name": "name",
    "value": "value"
    }
} ["Request body."]
```

# Serviços

Para exemplo e facilidade, alguns serviços já foram implementados, como o _TrackingService_ e o _QueryStringService_. 

__É importante seguir o padrão usado nesses serviços para as futuras implementações de serviços.__

## Query String Service

Dados recebidos por query, podem ser tratados utilizando o serviço de _Query String_ já implementado e pode ser usado da seguinte forma:

```typescript
import { QueryStringService as _queryStringService } from "../../services/queryStringService/QueryStringService";
```

E chamar a função de `.Parse` para recuperar os dados da _query string_:

```typescript
var values = _queryStringService.Parse();
```

Por exemplo pode ser usado da seguinte maneira, adicionado os parametros a url `http://localhost:3000?id=...`

```
?id=1&name=teste&value=value
``` 

Gerando um _json_ da seguinte forma:

```json
{
    "id": 1,
    "name": "teste",
    "value": "value",
}
```

Para mais informações veja o arquivo de [_QueryStringService_](src/services/queryStringService/QueryStringService.ts).

# Pipeline

## Docker

O _dockerfile_ foi ajustado para construir o _build_ de acordo com o ambiente que estiver sendo executado. Como pode ser visto abaixo:

```docker
...
# Environment will be replaced in pipeline
RUN ["npm", "run", "build:#{environment}#"]
...
```

> A variável _environment_ vai ser substituida durante a execução do pipeline.

# Links úteis

- [Learn React](https://reactjs.org)
- [Material UI](https://mui.com/pt/material-ui/getting-started/overview/)